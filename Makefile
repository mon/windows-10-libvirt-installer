NAME = windows-10-libvirt-installer

default:

install:
	mkdir -p $(DESTDIR)$(PREFIX)/usr/lib/$(NAME)
	cp -r data-iso $(DESTDIR)$(PREFIX)/usr/lib/$(NAME)/
	install -D -p -m 755 $(NAME) $(DESTDIR)$(PREFIX)/usr/bin/$(NAME)
