# https://gitlab.com/mon/windows-stagekicker

Start-Transcript -Path "C:\Windows\Temp\windows-stagekicker.log" -Append

$stagesPath = "$PSScriptRoot\stages"
$statePath = "c:\Windows\Temp"
$hookBreakPath = "$statePath\windows-stagekicker-hook-break"

$md5 = New-Object -TypeName System.Security.Cryptography.MD5CryptoServiceProvider
$utf8 = New-Object -TypeName System.Text.UTF8Encoding

Get-ChildItem -Path $stagesPath | ?{ $_.PSIsContainer } | ForEach-Object {
    $stageContainer = $_
    $stageName = $stageContainer.Name
    $stageId = [System.BitConverter]::ToString($md5.ComputeHash($utf8.GetBytes($stageContainer.FullName)))
    $stageStatusPath = "$statePath\$stageId-done"

    if (!(Test-Path -Path $stageStatusPath)) {
        echo "Running stage: $stageName ($stageId)"

        dir -Filter *.bat -Path $stageContainer.FullName | ForEach-Object {
            echo $_
            Get-Date
            Start-Process -FilePath $_.FullName -Wait -NoNewWindow
        }

        dir -Filter *.ps1 -Path $stageContainer.FullName | ForEach-Object {
            Get-Date
            powershell.exe -ExecutionPolicy Bypass -File $_.FullName | Out-Default
        }

        if (Test-Path -Path $hookBreakPath) {
            echo "hook: Break"
            break
        }

        New-Item -ItemType file -Path $stageStatusPath

        if (Test-Path -Path "$($stageContainer.FullName)\restart") {
            echo "Stage completed: $stageName (restarting computer)"
       	    Restart-Computer -Force
            break
        }

        if (Test-Path -Path "$($stageContainer.FullName)\will-restart") {
            echo "Stage completed: $stageName (waiting for restart)"
            break
        }

        echo "Stage completed: $stageName"
    } else {
        echo "Skipping stage: $stageName ($stageId - $stageStatusPath)"
    }
}

Stop-Transcript
